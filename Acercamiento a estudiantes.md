Este ítem serán charlas dirigidas a estudiantes de colegio, inicialmente a jóvenes de grados 10 y 11, próximos a graduarse. Tendrán como objetivo, introducir la idea o fin inicial buscado por el proyecto y ofrecer una visión a los jóvenes de las posibilidades que ofrece el mundo.

Estas charlas tendrán como fin:
- Discutir la tendencia actual del mundo.
- Posibilidades salariales de profesiones en la actualidad.
- La importancia de la tecnología y el papel que desempaña en la sociedad y en el rumbo del mundo.
- La importancia del conocimiento y la era del conocimiento.
- Propuesta de desarrollo del programa.
